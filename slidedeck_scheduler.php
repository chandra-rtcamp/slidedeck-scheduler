<?php

if ( !defined( "SLIDEDECK3_SCHEDULER_DIRNAME" ) )
	define( "SLIDEDECK3_SCHEDULER_DIRNAME", dirname( __FILE__ ) );
if ( !defined( "SLIDEDECK3_SCHEDULER_URLPATH" ) )
	define( "SLIDEDECK3_SCHEDULER_URLPATH", trailingslashit( plugins_url() ). 'slidedeck-addons/' . basename( SLIDEDECK3_SCHEDULER_DIRNAME ) );
if ( !defined( "SLIDEDECK3_SCHEDULER_VERSION" ) )
	define( "SLIDEDECK3_SCHEDULER_VERSION", "3.0.0" );

if ( !class_exists( 'SlideDeckPluginScheduler' ) ) {

	class SlideDeckPluginScheduler {

		var $namespace = "slidedeck-scheduler";
		var $package_slug = 'scheduler';
		static $st_friendly_name = "SlideDeck 3 Scheduler Addon";

		function __construct()
		{
			// Fail silently if SlideDeck core is not installed
			if ( !class_exists( 'SlideDeckPlugin' ) ) {
				return false;
			}

			SlideDeckPlugin::$addons_installed[$this->package_slug] = $this->package_slug;

			$this->slidedeck_namespace = SlideDeckPlugin::$st_namespace;

			/**
			 * Make this plugin available for translation.
			 * Translations can be added to the /languages/ directory.
			 */
			load_plugin_textdomain( $this->slidedeck_namespace, false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

			add_action( 'admin_print_scripts-toplevel_page_' . SLIDEDECK2_HOOK, array( &$this, 'admin_print_scripts' ) );
			add_action( 'admin_print_styles-toplevel_page_' . SLIDEDECK2_HOOK, array( &$this, 'admin_print_styles' ) );
			add_action( 'init', array( &$this, 'wp_register_scripts' ), 2 );
			add_action( 'init', array( &$this, 'wp_register_styles' ), 2 );
			add_action( "{$this->slidedeck_namespace}_setup_options_bottom", array( &$this, 'scheduler_options_model' ), 99, 2 );
		}

		/*
		 *  hook into global options
		 */

		function scheduler_options_model( $slidedeck )
		{

			$scheduler_options = array(
				'schedule_slider' => array(
					'type' => 'radio',
					'data' => "boolean",
					'value' => false,
					'label' => "Schedule Slider",
					'description' => "Helps to to set start date and end date for this slider."
				),
				'schedule_start_date' => array(
					'type' => 'text',
					'visible' => false,
					'label' => "Select start date",
					'description' => "Select start date for this slider",
					'attr' => array(
						'class' => "slidedeck-date-picker"
					)
				),
				'schedule_end_date' => array(
					'type' => 'text',
					'visible' => false,
					'label' => "Select end date",
					'description' => "Select end date for this slider",
					'attr' => array(
						'class' => "slidedeck-date-picker"
					)
				),
			);

			// check previous settings

			if ( isset( $slidedeck['options']['schedule_slider'] ) && $slidedeck['options']['schedule_slider'] ) {
				$scheduler_options['schedule_start_date']['visible'] = true;
				$scheduler_options['schedule_end_date']['visible'] = true;
			}

			foreach ( $scheduler_options as $name => $option ) {
				$is_visible = true;

				if ( array_key_exists( 'visible', $option ) ) {
					if ( $option['visible'] == false )
						$is_visible = false;
				}

				$html = "";

				$html .= $is_visible ? "<li>" : "<li style='visibility: hidden;'>";

				$input_html = "";

				if ( !isset( $option['attr']['class'] ) )
					$option['attr']['class'] = "";
				$option['attr']['class'].= " fancy";

				if ( array_key_exists( 'type', $option ) ) {
					$input_html.= slidedeck2_html_input( "options[$name]", $slidedeck['options'][$name], $option, false );
				}

				$html .= $input_html;
				$html .= "</li>";

				echo $html;
			}

			foreach ( $scheduler_options as $key => $value ) {
				$options_model['Setup'][$key] = $value;
			}
		}
		
		/**
		 * Load styles for the admin options page
		 *
		 * @uses wp_enqueue_style()
		 */
		function admin_print_styles()
		{
			wp_enqueue_style( "{$this->namespace}-jquery-ui" );
		}

		/**
		 * Load JavaScript for the admin options page
		 *
		 * @uses wp_enqueue_script()
		 */
		function admin_print_scripts()
		{
			wp_enqueue_script( "{$this->namespace}-admin" );
			wp_enqueue_script( "jquery-ui-datepicker" );
		}

		/**
		 * Initialization function to hook into the WordPress init action
		 *
		 * Instantiates the class on a global variable and sets the class, actions
		 * etc. up for use.
		 */
		static function instance()
		{
			global $SlideDeckPluginScheduler;

			$slidedeck2_version = defined( 'SLIDEDECK2_VERSION' ) ? SLIDEDECK2_VERSION : "2.0";

			if ( version_compare( $slidedeck2_version, '1.0', ">=" ) ) {
				// Only instantiate the Class if it hasn't been already
				if ( !isset( $SlideDeckPluginScheduler ) )
					$SlideDeckPluginScheduler = new SlideDeckPluginScheduler();
			}
		}
		
		/**
		 * Register styles used by this plugin for enqueuing elsewhere
		 *
		 * @uses wp_register_style()
		 */
		function wp_register_styles()
		{
			wp_register_style( "{$this->namespace}-jquery-ui", SLIDEDECK3_SCHEDULER_URLPATH . "/css/jquery-ui.css");
		}
		
		/**
		 * Register scripts used by this plugin for enqueuing elsewhere
		 *
		 * @uses wp_register_script()
		 */
		function wp_register_scripts()
		{
			wp_register_script( "{$this->namespace}-admin", SLIDEDECK3_SCHEDULER_URLPATH . "/js/admin" . ( SLIDEDECK2_ENVIRONMENT == 'development' ? '.dev' : '' ) . ".js", array( 'jquery' ), SLIDEDECK3_SCHEDULER_VERSION, true );
		}

	}
}
